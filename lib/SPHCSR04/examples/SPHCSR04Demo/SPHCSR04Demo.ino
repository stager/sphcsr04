#include <SPHCSR04.h>

const int TRIG_PIN = 12;
const int ECHO_PIN = 13;

SPHCSR04 distanceSensor(TRIG_PIN,ECHO_PIN);

void setup()
 {
  Serial.begin(9600);

  distanceSensor.startSensing();
 }

void loop()
 {
  if (distanceSensor.update())
   {
    if      (distanceSensor.isStuck())
     {
      Serial.println("Stuck");
     }
    else if (!distanceSensor.isOutOfRange())
     {
      Serial.print(distanceSensor.getDistance());
      Serial.println(" m");
     }
    else
     {
      Serial.println("Out of range");
     }
   }

  delay(20);
 }

