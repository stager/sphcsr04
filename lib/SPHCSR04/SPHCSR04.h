#ifndef __SPHCSR04_H__
#define __SPHCSR04_H__

#include <Arduino.h>

class SPHCSR04
 {
  public :

                   SPHCSR04           (int                 triggerPin,
                                       int                 echoPin);
  void             startSensing       ();
  void             stopSensing        ();
  bool             isActive           () const;

  bool             update             ();

  bool             isOutOfRange       () const;
  bool             isStuck            () const;
  float            getDistance        () const;

  float            getMaxDistance     () const;

  private :

  bool             isReadyToPing      () const;
  void             sendPing           ();
  void             readResult         ();

  unsigned long    lastPingTime;
  float            lastResult;
  bool             outOfRange;
  bool             stuck;

  byte             triggerPin;
  byte             echoPin;
  bool             active;

  static const unsigned long MIN_PING_PERIOD_IN_MS = 60;
  static const unsigned long PING_DURATION_IN_US   = 10;
 };

#endif

