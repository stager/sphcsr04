#include "SPHCSR04.h"

#define SPHCSR04_MAX_DISTANCE   (4.0f)
#define SPHCSR04_SPEED_OF_SOUND (340.0f)
#define SPHCSR04_TIMEOUT_IN_US \
 ((unsigned long)(((SPHCSR04_MAX_DISTANCE) / (SPHCSR04_SPEED_OF_SOUND)) * 1000000) * 2)
#define SPHCSR04_STUCK_RESET_TIME_IN_MS (2)

SPHCSR04::SPHCSR04                    (int                 triggerPin,
                                       int                 echoPin)
 {
  this->triggerPin = triggerPin;
  this->echoPin    = echoPin;

  active       = false;
  lastPingTime = 0;
  lastResult   = SPHCSR04_MAX_DISTANCE;
  outOfRange   = true;
  stuck        = false;
 }

void
SPHCSR04::startSensing                ()
 {
  if (active) return;

  pinMode(triggerPin,OUTPUT);
  pinMode(echoPin,INPUT);

  active = true;

  update();
 }

void
SPHCSR04::stopSensing                 ()
 {
  active = false;
 }

bool
SPHCSR04::isActive                    () const
 {
  return active;
 }

bool
SPHCSR04::update                      ()
 {
  if (!active) return false;

  if (isReadyToPing())
   {
    sendPing();
    readResult();

    return true;
   }
  else
   {
    return false;
   }
 }

bool
SPHCSR04::isReadyToPing               () const
 {
  return millis() - lastPingTime >= MIN_PING_PERIOD_IN_MS;
 }

void
SPHCSR04::sendPing                    ()
 {
  digitalWrite(triggerPin,HIGH);
  delayMicroseconds(PING_DURATION_IN_US);
  digitalWrite(triggerPin,LOW);

  lastPingTime = millis();
 }

void
SPHCSR04::readResult                  ()
 {
  unsigned long duration = pulseIn(echoPin,HIGH,SPHCSR04_TIMEOUT_IN_US);

  if (duration > 0)
   {
    lastResult = duration / 2 * SPHCSR04_SPEED_OF_SOUND / 1e6f;
    outOfRange = false;
    stuck      = false;
   }
  else
   {
    lastResult = SPHCSR04_MAX_DISTANCE;
    outOfRange = true;
    stuck      = false;

    if (digitalRead(echoPin) == HIGH)
     {
      pinMode(echoPin,OUTPUT);
      digitalWrite(echoPin,LOW);
      delay(SPHCSR04_STUCK_RESET_TIME_IN_MS);
      pinMode(echoPin,INPUT);

      stuck = digitalRead(echoPin) == HIGH;
     }
   }
 }

bool
SPHCSR04::isOutOfRange                () const
 {
  return outOfRange;
 }

bool
SPHCSR04::isStuck                     () const
 {
  return stuck;
 }

float
SPHCSR04::getDistance                 () const
 {
  return lastResult;
 }

float
SPHCSR04::getMaxDistance              () const
 {
  return SPHCSR04_MAX_DISTANCE;
 }

